import sympy

def carmichael(n):
    if n == 1 or sympy.isprime(n):
        return False

    factors = []
    m = n
    primes = sympy.primerange(2, m / 2 + 1)
    for prime in primes:
        while m % prime == 0:
            m = m / prime
            factors.append(prime)
        if m == 1:
            break

    # use factors not in n to construct relative primes.
    primes = set(primes) - set(factors)
    primes = list(primes)

    # check that all relative primes are not witnesses.
    return check(primes, 1, n)

def check(primes, m, n):
    for prime in primes:
        x = prime * m
        if x >= n:
            break
        if x**(n-1) % n != 1 or not check(primes, x, n):
            return False

    return True

n = int(input('number: '))
print(carmichael(n))
